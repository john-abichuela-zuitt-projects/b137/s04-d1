package b137.abichuela.s04d1;

public class Car {
    // Properties
    private String name;
    private String brand;
    private int yearOfMake;

    // Constructor

    // Empty constructor
    public Car() {}

    // Parameterized constructor
    private Car(String name, String brand, int yearOfMake) {
        this.name = name;
        this.brand = brand;
        this.yearOfMake = yearOfMake;
    }

    // Getters
    public String getName(){
        return name;
    }

    // Setters
    public void setName(String newName) {
        this.name = newName;
    }

    // Methods
    public void drive() {
        System.out.println("Driving the " + this.name);
    }
}
