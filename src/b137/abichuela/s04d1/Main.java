package b137.abichuela.s04d1;

public class Main {

    public static void main(String[] args) {
        Car firstCar = new Car();

        firstCar.setName("Suzuki");

        firstCar.drive();

        System.out.println(firstCar.getName());
    }
}
